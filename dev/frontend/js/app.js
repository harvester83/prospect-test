import $ from 'jquery';
import 'bootstrap';
import 'bootstrap-datepicker';
import 'eonasdan-bootstrap-datetimepicker';
import Scrollbar from 'smooth-scrollbar';

$(document).ready(function () {
    const fieldTitle = $('.field__title');
    const fieldSlid = $('.field-slid');

    fieldTitle.each(function () {
            $(this).on('click', function () {
            let clickbtn = $(this);

            $('.field__title').not(clickbtn).each(function(){
                $(this).next('.field-slid').removeClass('open');
                $(this).removeClass('open');
            });

            $(this).next('.field-slid').toggleClass('open');
            $(this).toggleClass('open');
        });
    });

    $(document).mouseup(function (e) {
        if (!fieldTitle.is(e.target) &&
            fieldTitle.has(e.target).length === 0 &&
            !fieldSlid.is(e.target) &&
            fieldSlid.has(e.target).length === 0) {

            fieldTitle.next('.field-slid').removeClass('open');
            fieldTitle.removeClass('open');
        }
    });

    $('.field-select__span').on('click', function () {
        let title = $(this).text();

        $(this).closest('.field-slid')
            .prev('.field__title')
            .find('.field__text').text(title);
    });

    $('#calendar input').datepicker({
        format: 'dd.mm.yyyy'
    });

    $('#hour').datetimepicker({
        format: 'LT'
    });

    $('#fromDay input').datepicker({
        format: 'dd.mm.yyyy'
    });

    $('#fromHour').datetimepicker({
        format: 'LT'
    });
    $('#toDay input').datepicker({
        format: 'dd.mm.yyyy'
    });

    $('#toHour').datetimepicker({
        format: 'LT'
    });

    Scrollbar.init(document.querySelector('.table-theme-block'), {
        alwaysShowTracks: true,
    });

    const thumbnailPhotoCircle = $('.thumbnail-photo-circle');
    const thumbnailPhotoBlock = $('.thumbnail-photo-block');

    thumbnailPhotoCircle.on('click', function () {
        thumbnailPhotoBlock.toggleClass('active')
    });

    $(document).mouseup(function (e) {
        if (!thumbnailPhotoCircle.is(e.target) &&
            thumbnailPhotoCircle.has(e.target).length === 0 &&
            !thumbnailPhotoBlock.is(e.target) &&
            thumbnailPhotoBlock.has(e.target).length === 0) {

            thumbnailPhotoBlock.removeClass('active')
        }
    });

    $('.thumbnail-photo__badge').on('click', function () {
        let id = $(this).attr('data-number');
        $('.thumbnail-photo').filter(function(){
            return $(this).attr('data-number') === id
        }).remove();
    })
});
